import { useState } from 'react';

function SideBar() {
  // const [visible, setVisible] = useState(false)
  // const handleShow = () => {
  //   setVisible(!visible)

  // }
  return (
    // <div style={{ position: 'relative' }}>
    //   <button className="btn button_afficher" onClick={handleShow}><ion-icon name="menu-outline" className="shadow-sm" />  </button>

    //   {  
    //    visible &&
     <div className="navigation" >


          <ul >
            <li>
              <a href="#">
                <span className="icon">
                  <ion-icon name="logo-medium" />

                </span>
                <span className="title">MAM EXPRESS</span>
              </a>
            </li>
            <li>
              <a href="/dashboard">
                <span className="icon">
                  <ion-icon name="home-outline" />
                </span>
                <span className="title">Dashboard</span>
              </a>
            </li>
            <li>
              <a href="/customers">
                <span className="icon">
                  <ion-icon name="people-outline" />
                </span>
                <span className="title">Customers</span>
              </a>
            </li>
            <li>
              <a href="/package">
                <span className="icon">
                  <ion-icon name="cart-outline" />
                </span>
                <span className="title">Package</span>
              </a>
            </li>
            <li>
              <a href="/invoices">
                <span className="icon">
                  <ion-icon name="documents-outline" />
                </span>
                <span className="title">Invoices</span>
              </a>
            </li>
            <li>
              <a href="/return">
                <span className="icon">
                  <ion-icon name="return-down-back-outline" />
                </span>
                <span className="title">Return Package</span>
              </a>
            </li>
            <li>
              <a href="/pickup">
                <span className="icon">
                  <ion-icon name="albums-outline" />
                </span>
                <span className="title">Pickup</span>
              </a>
            </li>

          </ul>
        </div>
      // }

    // </div>


  )
}



export default SideBar;
